import java.util.*;

public class Classroom {
    private TreeSet<Student> studentsByName = new TreeSet<Student>(new StudentNameComparator());
    private HashMap<SubjectType, SchoolSubject> subjects = new HashMap<>();
    private ClassName className;

    public Classroom(ClassName className) {
        this.className = className;
    }

    public ClassName getClassName() {
        return className;
    }

    public void setClassName(ClassName className) {
        this.className = className;
    }

    public void addStudent(String pesel, String name, String surname){
        studentsByName.add(new Student(pesel, name, surname));
    }

    public void addSubject(SubjectType TYPE){
        subjects.put(TYPE, new SchoolSubject(TYPE));
    }

    public void addGradeToSubject(Integer studentID, SubjectType subjectType, GradeType gradeType, float grade){
        try {
            subjects.get(subjectType).addGrade(studentID, new Grade(grade, gradeType));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printAllStudents(){
        for (Student s : studentsByName) {
            System.out.println(s);
        }
    }

    private class StudentNameComparator implements Comparator<Student>{
        @Override
        public int compare(Student o1, Student o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
