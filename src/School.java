import java.util.HashMap;

public class School {
    private HashMap<ClassName, Classroom> classroomDatabase = new HashMap<>();

    public void addClass(ClassName className){
        classroomDatabase.put(className, new Classroom(className));
    }

    public boolean checkClassExistence(ClassName className){
        return classroomDatabase.containsKey(className);
    }

    public void printClassStudents(ClassName className){
        classroomDatabase.get(className).printAllStudents();
    }
}
