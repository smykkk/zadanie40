import java.util.HashMap;
import java.util.List;

public class SchoolSubject extends Classroom{
    private HashMap<Integer, List<Grade>> grades = new HashMap<>();
    SubjectType TYPE;

    public SchoolSubject(SubjectType TYPE) {
        this.TYPE = TYPE;
    }

    public void addGrade(Integer id, Grade g){
        grades.get(id).add(g);
    }

    public void printAllGrades(){
        for (Integer i :grades.keySet()) {
            System.out.println(grades.get(i));
        }
    }

    public void printStudentGrades(Integer id){
        System.out.println(grades.get(id));
    }

}
