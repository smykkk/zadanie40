public class Grade {
    private float grade;
    GradeType gradeType;

    public Grade(float grade, GradeType gradeType) {
        this.grade = grade;
        this.gradeType = gradeType;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "grade=" + grade +
                ", gradeType=" + gradeType +
                '}';
    }
}
